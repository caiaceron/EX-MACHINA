﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Shoot : MonoBehaviour
{
    [SerializeField]
    Shoot_Manager plasmaRifle;

    Animator anim;
    

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetButton("Fire"))
        {
            plasmaRifle.Fire();
        }
       

    }

}
