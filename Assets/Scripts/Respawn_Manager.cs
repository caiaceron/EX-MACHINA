﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn_Manager : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject playerPrefab;



    void OnEnable()
    {
        Player_Manager.OnPlayerKilled += SpawnPlayer;
    }

    void SpawnPlayer()
    {
        Instantiate(playerPrefab, spawnPoint.position, Quaternion.identity);
    }
}
