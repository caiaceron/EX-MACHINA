﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_States : MonoBehaviour
{
   public enum E_MoveStates
    {
        WALKIN,
        RUNNING,
        JUMPING,
        SPRINTING

    }

    public enum E_WeaponState
    {
        IDLE,
        FIRING,
        AIMING,
        AIMFIRING
    }

    public E_MoveStates MoveStates;
    public E_WeaponState WeaponStates;


    void Update()
    {
        SetMoveStates();
        SetWeaponStates();
    }

    void SetWeaponStates()
    {
        WeaponStates = E_WeaponState.IDLE;
        if (Input.GetButton("Fire"))
        {
            WeaponStates = E_WeaponState.FIRING;
        }
        if (Input.GetButton("Aim"))
        {
            WeaponStates = E_WeaponState.AIMING;
        }
        if(Input.GetButton("Fire") && Input.GetButton("Aim"))
        {
            WeaponStates = E_WeaponState.AIMFIRING;
        }
        
    }

    void SetMoveStates()
    {
        MoveStates = E_MoveStates.RUNNING;

        if (Input.GetButton("Sprint"))
        {
            MoveStates = E_MoveStates.SPRINTING;
        }
        if (Input.GetButton("Jump"))
        {
            MoveStates = E_MoveStates.JUMPING;
        }
      
    }
}
