﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour {

    //Referencia aos Objetos
    public Transform cam;
    CharacterController mover;
    Animator anim;

    //Camera
    Vector3 camForward;
    Vector3 camRight;


    //Inputs
    Vector2 input;

    //Fisica
    Vector3 moveVector;
    Vector3 lastMove;
    Vector3 intent;
    Vector3 velocityXZ;
    Vector3 velocity;
    float speed = 8;
    float accel = 15;
    float turnSpeed = 5;
    float turnSpeedLow = 5;
    float turnSpeedHigh = 20;
    float JumpForce = 9.8f;

    //Gravidade
    float grav = 25.0f;
    public bool isGrounded = false;


    void Start()
    {
        mover = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        
        
        Move();

        mover.Move(velocity * Time.deltaTime);

        Gravity();

        CalculateGround();

        Input();

        Camera();

        DoAnimation();


    }

    void Move()
    {
        float tS = velocity.magnitude / 5;
        turnSpeed = Mathf.Lerp(turnSpeedHigh, turnSpeedLow, tS);


        intent = camForward * input.y + camRight * input.x;

        if (input.magnitude > 0)
        {
            Quaternion rot = Quaternion.LookRotation(intent);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, turnSpeed * Time.deltaTime);
        }

        velocityXZ = velocity;
        velocityXZ.y = 0;
        velocityXZ = Vector3.Lerp(velocityXZ, transform.forward * input.magnitude * speed, accel * Time.deltaTime);
        velocity = new Vector3(velocityXZ.x, velocity.y, velocityXZ.z);
    }

    void Gravity()
    {
        
        if (mover.isGrounded)
        {
            velocity.y = -grav * Time.deltaTime;
            if (Input_Manager.Button_A())
            {
                velocity.y = JumpForce;
            }
        }
        else
        {
            velocity.y -= grav * Time.deltaTime;
            velocityXZ = lastMove;
        }
        velocityXZ.y = 0;
        velocityXZ.Normalize();
        velocityXZ *= speed;
        velocityXZ.y = velocity.y;

        mover.Move(moveVector * Time.deltaTime);
        lastMove = velocityXZ;

    }

    void Input()
    {
        input = new Vector2(Input_Manager.MoveHorizontal(), Input_Manager.MoveVertical());
        input = Vector2.ClampMagnitude(input, 1);
    }

    void CalculateGround()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up * 0.1f, - Vector3.up, out hit, 0.2f))
        {
            mover.Move(new Vector3(0, -hit.distance, 0));
            isGrounded = true;
            
        }
        else
        {
            isGrounded = false;
            
        }


    }
    
    void Camera()
    {
        camForward = cam.forward;
        camRight = cam.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    void DoAnimation()
    {
        if (input.magnitude != 0)
        {
            anim.SetInteger("isRunning", 1);
        }
        else
        {
            anim.SetInteger("isRunning", 0);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!mover.isGrounded && hit.normal.y < 0.1f)
        {
            if (Input_Manager.Button_A())
            {
                Debug.DrawRay(hit.point, hit.normal, Color.red, 1.25f);
                velocity.y = JumpForce;
                velocityXZ = hit.normal * speed;
            }
        }
    }
}
