﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{

    Animator anim;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            anim.SetInteger("Firing", 1);
        }
        else
        {
            anim.SetInteger("Firing", 0);
        }

        if (Input.GetMouseButtonDown(1))
        {
            anim.SetBool("Aim", true);

        }
        else if(Input.GetMouseButtonUp(1))
        {
            anim.SetBool("Aim", false);
        }
        
    }
}
