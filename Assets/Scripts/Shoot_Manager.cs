﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot_Manager : MonoBehaviour
{
    [SerializeField]
    float fireRate;
    [SerializeField]
    Transform projectile;

    [HideInInspector]
    public Transform muzzle;

    float nextFireAllow;
     public bool canFire;


    void Awake()
    {
        muzzle = transform.Find("Muzzle");
    }


    void Start()
    {
       
    }

    public virtual void Fire()
    {
        canFire = false;

        if (Time.time < nextFireAllow)
        {
            return;
        }
      
        nextFireAllow = Time.time + fireRate;

        Instantiate(projectile, muzzle.position, muzzle.rotation);
        canFire = true;
    }
   
}
