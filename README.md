# EX-MACHINA

Jogo de ação/aventura side scroller 3d


## Pré-requisitos

* Sistema Operacional Windows 7 ou superior.
* Programa Descompactador de Arquivos(Ex: WinRAR / 7-Zip).
* API Gráfica DirectX.
* Requisitos do Sistema(A definir).


## Instalação

### Passo 1º
* Baixar o arquivo gameDemo.rar
* Disponivel na Branch Build.

### Passo 2º
* Extrair o arquivo no diretorio
* de sua escolha.

### Passo 3º
* Abri a Pasta do jogo.

### Passo 4º
* Executar o arquivo .exe.


### Link para baixar o jogo:
https://1drv.ms/u/s!AvsAbEKX6R4tlX8xwLfKwwZ-IO-m?e=7Bsfgg