﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Manager : MonoBehaviour
{
    //Debug Section

   
    public bool debugAim;
    //Debug Section

    //Player Variables
    [SerializeField] [Range(0, 10)] public float jumpForce = 4.0f;
    [SerializeField] [Range(0, 10)] public float fallMulti = 1.0f;
    [SerializeField] [Range(0, 10)] public float lowJumpMulti = 2.0f;

    [SerializeField] [Range(0, 100)] public float sensitivityX = 2.0f;
    [SerializeField] [Range(0, 100)] public float sensitivityY = 2.0f;
    [SerializeField] [Range(0, 10)] public float dampingX = 1.0f;
    [SerializeField] [Range(0, 10)] public float dampingY = 1.0f;


    public HealthBar healthBar;
    //Player Variables


    //References
    Rigidbody rb;
    Animator anim;
    CapsuleCollider cc;
    private Crosshair m_crosshair;

    //References


    private Crosshair Crosshair
    {
        get
        {
            if (m_crosshair == null)
            {
                m_crosshair = GetComponentInChildren<Crosshair>();
            }
            return m_crosshair;
        }
    }

    public delegate void PlayerKilled();
    public static event PlayerKilled OnPlayerKilled;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cc = GetComponent<CapsuleCollider>();
        anim = GetComponent<Animator>();
        m_crosshair = GetComponentInChildren<Crosshair>();
     
    }

    void FixedUpdate()
    {
        Move();
        MoveAiming();
        Run();
        Jump();

    }


    void Update()
    {


        if (Input.GetKeyDown(KeyCode.K) || healthBar.health <= 0)
        {
            Die();
            healthBar.Refill(100);

        }
        if (debugAim)
        {
            anim.SetBool("aiming", true);
        }

    }


    void Move()
    {
        anim.SetFloat("vertical", Input.GetAxis("Vertical"));
        anim.SetFloat("horizontal", Input.GetAxis("Horizontal"));
    }

    void MoveAiming()
    {
        if (Input.GetButton("Aim"))
        {
            anim.SetBool("aiming", true);


        }
        else
        {
            anim.SetBool("aiming", false);

        }
    }

    void Run()
    {
        if (anim.GetFloat("vertical") >= 0.5f && Input.GetButton("Sprint"))
        {
            anim.SetBool("sprinting", true);
        }
        else
        {
            anim.SetBool("sprinting", false);
        }


    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rb.velocity = Vector3.up * jumpForce;

            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector3.up * Physics.gravity.y * (fallMulti - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMulti - 1) * Time.deltaTime;

            }
            anim.SetBool("jumping", true);
        }
        else
        {
            anim.SetBool("jumping", false);
        }

    }


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Fist")
        {
            print("vei até aqui");
            if (healthBar)
            {
                healthBar.OnTakeDamage(10);
                print("Toma Porra!");
            }
        }
    }

    void Die()
    {
        Destroy(gameObject);
        if (OnPlayerKilled != null )
        {
            OnPlayerKilled();
        }
    }

}
