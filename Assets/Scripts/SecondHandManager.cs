﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class SecondHandManager : MonoBehaviour
{
    public AimIK aimIK;
    public FullBodyBipedIK ik;
    public LookAtIK look;

    private IKEffector LeftHand { get { return ik.solver.leftHandEffector; } }
    private IKEffector RightHand { get { return ik.solver.rightHandEffector; } }

    private Quaternion leftHandRotationRelative;

    void Start()
    {
        aimIK.enabled = false;
        ik.enabled = false;
        look.enabled = false; 

        ik.solver.OnPostUpdate += OnPostFBBIK;
    }

    void LateUpdate()
    {
        Vector3 toLeftHand = LeftHand.bone.position - RightHand.bone.position;
        Vector3 toLeftHandRelative = RightHand.bone.InverseTransformDirection(toLeftHand);

        leftHandRotationRelative = Quaternion.Inverse(RightHand.bone.rotation) * LeftHand.bone.rotation;

        aimIK.solver.IKPosition = look.solver.IKPosition;

        aimIK.solver.Update();

        LeftHand.position = RightHand.bone.position + RightHand.bone.TransformDirection(toLeftHand);
        LeftHand.positionWeight = 1.0f;

        RightHand.position = RightHand.bone.position;
        RightHand.positionWeight = 1.0f;
        ik.solver.GetLimbMapping(FullBodyBipedChain.RightArm).maintainRotationWeight = 1.0f;

        ik.solver.Update();

        look.solver.Update();


    }

    private void OnPostFBBIK()
    {
        LeftHand.bone.rotation = RightHand.bone.rotation * leftHandRotationRelative;
    }

}
