﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Third_Person_Camera : MonoBehaviour
{
    [SerializeField] Vector3 cameraOffset;
    [SerializeField] float damping;

    [SerializeField]
    public Transform player;

    Transform cameraTarget;


    void Start()
    {

        Cursor.lockState = CursorLockMode.Locked;
        cameraTarget = player.transform.Find("Camera Target");
        if(cameraTarget == null)
        {
            cameraTarget = player.transform;
        }

    }

    void Update()
    {
        transform.Rotate(Vector3.right * Input.GetAxisRaw("Mouse Y"));
        Vector3 targetPosition = cameraTarget.position + player.transform.forward * cameraOffset.z + player.transform.up * cameraOffset.y + player.transform.right * cameraOffset.x;

        Quaternion targetRotationX = Quaternion.LookRotation(cameraTarget.position - targetPosition, Vector3.up);
     
        transform.position = Vector3.Lerp(transform.position, targetPosition, damping * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotationX, damping * Time.deltaTime);


    }




}
