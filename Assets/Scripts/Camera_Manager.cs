﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Manager : MonoBehaviour
{
    [System.Serializable]
    public class CameraRig
    {
        public Vector3 cameraOffset;
        public float damping;
    }

    [SerializeField]
    CameraRig defaultCamera;
    [SerializeField]
    CameraRig aimingCamera;


    private const float Y_MAX_ANGLE = 30.0f;
    private const float Y_MIN_ANGLE = -30.0f;

    [SerializeField]
    [Range(0, 10)]
    private float sensivityX = 1.0f;

    [SerializeField]
    [Range(0, 10)]
    private float sensivityY = 1.0f;

    private float currentX = 0.0f;
    private float currentY = 0.0f;

    //References
    public Transform camTrans;
    public Transform target;
    public Transform pivot;
    //References

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        camTrans = this.transform;

    }

    void Update()
    {
        CameraController();
    }

    private void LateUpdate()
    {
        CameraPosition();
    }

    void CameraController()
    {
        currentX += Input.GetAxisRaw("Mouse X") * sensivityX;
        target.transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X"));

        currentY -= Input.GetAxisRaw("Mouse Y") * sensivityY;

        currentY = Mathf.Clamp(currentY, Y_MIN_ANGLE, Y_MAX_ANGLE);
    }

    void CameraPosition()
    {
        CameraRig cameraRig = defaultCamera;
        if (Input.GetButton("Aim") || Input.GetButton("Fire"))
        {
            cameraRig = aimingCamera;
        }


        cameraRig.cameraOffset = new Vector3(cameraRig.cameraOffset.x, cameraRig.cameraOffset.y, cameraRig.cameraOffset.z);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTrans.position = pivot.position + rotation * cameraRig.cameraOffset;
        camTrans.LookAt(pivot.position);
    }
}
