﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour
{
    public Image healthBar;
    public float health = 100.0f;
    public float startHealth = 100.0f;

    public void OnTakeDamage(int damage)
    {
        health = health - damage;
        healthBar.fillAmount = health / startHealth;
  
    }
    public void Refill(float startHealth)
    {
        if (health <= 0)
        {
           healthBar.fillAmount = startHealth;
        }
    }
}
