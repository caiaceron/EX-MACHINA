﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Input_Manager{

    //-- Eixos.

    //Recebe os Dois Metodos, dentro de um Vector3.
    public static Vector3 MainJoyStick()
    {
        return new Vector3(MoveHorizontal(), 0, MoveVertical());
    }


    //Movimenta o Personagem No Eixo Z.
    public static float MoveHorizontal()
    {
        float r = 0.0f;
        r += Input.GetAxis("LStickHorizontal");
        r += Input.GetAxis("KeyBoardHorizontal");
        return Mathf.Clamp(r, -1.0f, 1.0f);

    }

    //Movimenta o Personagem No Eixo X.
    public static float MoveVertical()
    {
        float r = 0.0f;
        r += Input.GetAxis("LStickVertical");
        r += Input.GetAxis("KeyBoardVertical");
        return Mathf.Clamp(r, -1.0f, 1.0f);
    }    

    //Rotaciona a Camera no Eixo X.
    public static float CamHorizontal()
    {
        float r = 0.0f;
        r += Input.GetAxis("RStickHorizontal");
        r += Input.GetAxis("Mouse X");
        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    //Rotaciona a Camera no Eixo Y.
    public static float CamVertical()
    {
        float r = 0.0f;
        r += Input.GetAxis("RStickVertical");
        r += Input.GetAxis("Mouse Y");
        return Mathf.Clamp(r, -1.0f, 1.0f);
    }


    //-- Buttões
    //(Controle do Xbox 360 Como Padrão).

    public static bool Button_A()
    {
        return Input.GetButtonDown("JoyButtonA");
    }

    public static bool Button_B()
    {
        return Input.GetButtonDown("JoyButtonB");
    }

    public static bool Button_X()
    {
        return Input.GetButtonDown("JoyButtonX");
    }

    public static bool Button_Y()
    {
        return Input.GetButtonDown("JoyButtonY");
    }

    public static bool Button_RB()
    {
        return Input.GetButtonDown("JoyButtonRB");
    }

    public static bool Button_LB()
    {
        return Input.GetButtonDown("JoyButtonLB");
    }

    public static bool Button_Back()
    {
        return Input.GetButtonDown("JoyBack");
    }

    public static bool Button_Start()
    {
        return Input.GetButtonDown("JoyStart");
    }

    public static bool Button_RSB()
    {
        return Input.GetButtonDown("RSButton");
    }

    public static bool Button_LSB()
    {
        return Input.GetButtonDown("LSButton");
    }

}
