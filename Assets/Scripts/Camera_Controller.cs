﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour {

    public Rails_Controller railsController;
    public Transform lookAt;
    public bool smoothMove = true;
    public float moveSpeed = 5.0f;


    private Transform thisTransform;
    private Vector3 lastPos;

    private void Start()
    {
        thisTransform = transform;
        lastPos = thisTransform.position;
    }

    private void LateUpdate()
    {
        if (smoothMove)
        {
            lastPos = Vector3.Lerp(lastPos, railsController.ProjectPositionOnRail(lookAt.position), moveSpeed * Time.deltaTime);
            thisTransform.position = lastPos;
        }
        else
        {
            thisTransform.position = railsController.ProjectPositionOnRail(lookAt.position);

        }
        thisTransform.LookAt(lookAt.position);
    }
}
