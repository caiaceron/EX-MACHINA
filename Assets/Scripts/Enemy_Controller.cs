﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Controller : MonoBehaviour {

    public float lookRadius = 10f;
    public Transform target;

    
    void Start ()
    {
       
    }

    void Update()
    {

        float distance = Vector3.Distance(target.position, transform.position);

        if ( distance <= lookRadius)
        {
        this.gameObject.GetComponent<NavMeshAgent>().destination = target.transform.position;

            FaceTarget();
        }

    }

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}

