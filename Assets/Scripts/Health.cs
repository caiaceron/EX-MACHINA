﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Destructable
{
    public override void Die()
    {
        base.Die();

        print("MORI JORGE!");
    }

    public override void TakeDamage(float amount)
    {
        print("Pontos de Vida Restante: " + HitPointsRemaining);
        base.TakeDamage(amount);
    }
}
