﻿using UnityEngine;

public class Sliding_Door2 : MonoBehaviour
{

    public GameObject trigger;
    public GameObject door;
    

    Animator doorAnim;
    
    void Start()
    {
        doorAnim = door.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            OpenDoor(true);
        }
    }

    private void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            OpenDoor(false);
        }
    }

    void OpenDoor(bool state)
    {
        doorAnim.SetBool("open", state);
        
    }

}