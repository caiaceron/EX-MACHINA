﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform camIK;
    public Transform pivot;
    public Transform target;
    public Vector3 offset;
    public bool useOffsetValues;
    public float rotateSpeed;

   const float MAX_ANGLE =  40.0f;
   const float MIN_ANGLE = 330.0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        if (!useOffsetValues)
        {
            offset = target.position = transform.position;
        }

        pivot.transform.position = target.transform.position;
        pivot.transform.parent = target.transform;


    }

    void LateUpdate()
    {

        float horizontal = Input.GetAxisRaw("Mouse X") * rotateSpeed;
        target.Rotate(0, horizontal, 0);

        float vertical = Input.GetAxisRaw("Mouse Y") * rotateSpeed;
        pivot.Rotate(-vertical, 0, 0);

        if (pivot.rotation.eulerAngles.x > 70f && pivot.rotation.eulerAngles.x < 180f)
        {
            pivot.rotation = Quaternion.Euler(70f, 0, 0);
        }

        if (pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 315f)
        {
            pivot.rotation = Quaternion.Euler(315f, 0, 0);
        }

        float desiredYAngle = target.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = target.position - (rotation * offset);

        if (transform.position.y < target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y, transform.position.z);
        }

        transform.LookAt(pivot);

        
        
    }
}
