﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Controller : MonoBehaviour
{
    public delegate void Enemykilled();
    public static event Enemykilled OnEnemyKilled;

    void Start()
    {
        SetRigidbodyState(true);
        SetColliderState(false);
        GetComponent<Animator>().enabled = true;
    }

    public void Die()
    {

        GetComponent<Animator>().enabled = false;
        SetRigidbodyState(false);
        SetColliderState(true);

        if (gameObject != null)
        {
            Destroy(gameObject, 10.0f);
        }

        if (OnEnemyKilled != null)
        {
            OnEnemyKilled();
        }

    }

    void SetRigidbodyState(bool state)
    {

        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;

    }


    void SetColliderState(bool state)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;

    }

}
