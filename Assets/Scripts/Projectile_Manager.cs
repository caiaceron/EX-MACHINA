﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class Projectile_Manager : MonoBehaviour
{

    [SerializeField] float speed;
    [SerializeField] float liveTime;
    [SerializeField] float damage;

    public GameObject explosive;

    void Start()
    {
        Destroy(gameObject, liveTime);
    }

    
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        Enemy_Controller enemy = other.transform.GetComponent<Enemy_Controller>();

        if (enemy != null)
        {
            enemy.Die();
        }

        if (other.gameObject.tag != "Player")
        {
            GameObject explosion = Instantiate(explosive, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(explosion, 2f);
            return;
        }

        var destructable = other.transform.GetComponent<Destructable>();

        if (destructable == null)
        {
            return;
        }

        destructable.TakeDamage(damage);
    }
    /*
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            GameObject explosion = Instantiate(explosive, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(explosion, 2f);
            return;
        }
    }
    */
}

                
